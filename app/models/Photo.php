<?php

namespace event\event_20140630_photo_upload;

class Photo extends \Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'photos';
    protected $fillable = array('width', 'height', 'url', 'sort', 'position', 'target', 'img_ver', 'user_id', 'state');
    protected $validateRules = array(
        'img_ver' => 'required',
    );

    public function comments() {


        return $this->hasMany('\event\event_20140630_photo_upload\Comment');
    }

}
