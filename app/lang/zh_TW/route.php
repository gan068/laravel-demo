<?php

return array(
    'comment_photo' => '對照片留言',
    'add_photo' => '新增照片',
    'remove_comment' => '刪除回應',
    'remove_photo' => '刪除照片',
    'list_photo' => '照片列表',
);
